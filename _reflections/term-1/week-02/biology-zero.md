---
title: 02. Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---
#02. BIOLOGY ZERO
#### by Jonathan Minchin and Núria Conde


###### ![]({{site.baseurl}}/DSC_0280.jpg) (Photo credit: Ryota Kamio)

## Hypothesis
#### Bacteria intelligence will be employed for environmental location decision making or geographical reasoning?
### What bacteria will do to you? - Bacteria help you find where to live.
##### ![]({{site.baseurl}}/idealista.jpg) (Source: Idealista)


#### When it comes to decision of a location to live, very often the environmental factors are not taken into account as much as other factors such as the rent price, access to facilities of your interest, etc...  

#### What if we observe more carefully our surroundings till the bacteria level so that we better our relationship between human and nature?  

## Method
1. Bench-marketing of working / case studies of cities born for environmental or geographical reasons
2. Classification of intelligent bacteria
3. Classification of genetically modified intelligent bacteria
4. Identification of tools to visualize the interaction between bacteria and human in a residence
5. Identification of the test area
6. Identification of the condition
7. Experimentation
8. Analysis of the result
9. Visualization of the result
10. Documentation

## Reflection
#### What did you do that is new. What did you learn?
#### Biology is the code of evolution and also the oldest and latest systemic design that exists in this planet. Bacteria has evolved to respond environmental changes, however we, human, have stopped to evolve ourselves at some point of the history. On the other hand, we have been protagonist of the environmental changes.

###### ![]({{site.baseurl}}/DSC_0356.jpg) (Photo credit: Ryota Kamio)


- Life : A piece of information that maintains itself and alive to next generations (Dr. Núria Conde).

- "Scientific literacy empowers everyone who possesses it to be active contributors to their own health care, the quality of their food, water, and air, their very interactions with their own bodies and the complex world around them." (M. Patterson 2010 in Biopunk Manifesto)

- "Synthetic biology is an interdisciplinary branch of biology and engineering. The subject combines disciplines from within these domains, such as biotechnology, genetic engineering, molecular biology, molecular engineering, systems biology, membrane science, biophysics, electrical engineering, computer engineering, control engineering and evolutionary biology. Synthetic biology applies these disciplines to build artificial biological systems for research, engineering and medical applications." (Wikipedia 15/10/2018)

- Farming is already human's activity (Dr. Núria Conde)

- What we think natural is largely not natural. Because we (human) have changed the ecosystem such as soil, water, bacteria, etc... (Dr. Núria Conde)

- We (human) can see until 0,1mm = 100 micrometers (Dr. Núria Conde)




### How can your learning process be described?

###### ![]({{site.baseurl}}/F+lhmkHaR9GXd0U9eEskSA_thumb_5bd.jpg) (Photo credit: Ryota Kamio)

- Learning biological methods
- Learning systemic design in biology
- Reframing
- Experiment
- Parallel thinking of application and consequence

### How is this relevant to your work?

Biological approach in many ways can be applied to what is around us everyday. For instance, in terms of urbanization process it clearly shows the potential of biological approach which recognizes a city as an ecosystem.

According to the Urban Ecology Agency of Barcelona, "Ecosystemic Urbanism seeks to overcome the restrictions of efficiency and habitability in the design of today's cities. These restrictors (green space, transportation etc.) must be considered in the development of a sustainable city in order to maximise the conservation of land and resources. Habitability is a term that refers to the livability of an area. It is linked to the comfort and interaction among residents: social cohesion, biological diversity, quality of public space, housing, equipment, etc."

###### ![]({{site.baseurl}}/IMG_2636.jpg) (Source: Ryota Kamio)

In order to maximise the conservation of the land and natural resources the difference of evolution in bacteria and cities should be taken into account. Often the bacteria has evolved responding to environmental changes and changing itself in many different ways. In other words the bacteria has been able to evolve transforming itself. However, we human have evolved cities expanding them instead of transforming ourselves. The expansion of the cities to unused land with more use of natural resources has caused serious environmental issues undoubtedly.

The evolution of cities must change their processes immediately, because we human are a part of an ecosystem called the earth and we've been destroying ourselves so bad for last decades.

### But how could we evolve our cities or ourselves?  

One of the possible alternative is creativity, since simply the expansion has the limit but creativity has no limit. Instead of growing larger in quantitative fashion we could evolve cities with creativity in qualitative fashion. This requires much observation of our cities and ourselves so that we could evolve our creativity. In this sense, there are a lot to learn from biological evolution we've learned from the Biology Zero class for an urbanist like myself and designers in general.      
