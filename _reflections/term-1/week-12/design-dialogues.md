---
title: 12. Design Dialogues
period: 17-23 December 2018
date: 2018-12-23 12:00:00
term: 1
published: true
---
#12. Design Dialogues
#### by Tomas Diez, Oscar Tomico and Mariana Quintero (MDEF faculty)

### Summary

This week we have presented the work of our area of interest we have researched throughout the first term. I presented "Futures of Living" as the area of my interest in a poster and a mini-documentary formats. At the exhibition I tried to explain from my path of learning to the decision of the area of my interest in each phase and got the feedbacks about it. Specifically during the Design Dialogue I got the feedbacks from Tomas Diez, Ingy Freyr, Lucas Peña, Guillem Campodron and Nuria Conde.  


### My path until now throwout the 1st term of MDEF

1First of all, I have been researching urban issues such as gentrification and housing market speculation, etc... as a geographer/urbanist before I joined IAAC. Precisely I joined the Master in Design for Emergent Futures (MDEF) to design solutions for the wicked issues associated with urban life I had been researching prior to starting the course. This 1st term has been a time of exploring a great variety of design theories and methodologies with lecturers inside/outside the IAAC, which has pushed me to become a "designer for emergent futures".

Through the lectures I have learned how society and design have interacted between each other and how designers begin to design his/her projects by different approaches. On the other hand, more practically speaking making narratives of your own project has been one of the biggest learnings for me, which helps develop my projects in sustainable way in the future. Also it has helped me to see my interest from different perspectives and on different scales.  

One of the difficulties that I have had during the 1st term is how to "personalize" the area of interest and "dive in" the theme.  


##### ![]({{site.baseurl}}/Learning process.png) (Credit: Ryota Kamio)

### Futures of Living

I was using the term Housing as my interest, however by reframing my interest I realized I'm not interested at house but the act of living. Therefore, here I use the term Living instead of Housing.

From the quantitative viewpoint, as the population continues to grow in cities and resources become even more depleted, the issues surrounding living are only going to get worse. Actually many cities are already facing massive issues due to lack of housing and in Africa and Asia it's going to be even more severe in the very near future. When the cities face such population growth in the history great architects and urban planners have designed or re-designed the city like Le Corbusier or Ildefons Cerdà did.

I believe that we, as individuals (= non-urban planners), also should participate actively in the process of re-desiging our city living. Today, the bottom up of creativity is a necessity when it comes to considering futures of city living, because we have limited land and energy in such limited space and apparently the expansion of a city is not an option for the lack of natural resources. For that reason I think co-creation and co-design with people of different profiles should enrich the content of the design for the futures of living from different perspectives than urbanistic or architectural ones. It's also because the relationship between yourself and others is the center of the discussion many times in terms of living in city environment, therefore it makes more sense to co-create and co-design with others.

On the other hand, I picked speculative and futuristic design as another methodology for my project. As I mentioned above, futures of living is a wicked problem and often the discussion ends up being too abstract to apply in real life, even though the act of living fundamentally impacts our everyday life. I believe that the speculative and futuristic design methodologies allow you to explore what you want tomorrow to look like more than what you can do, which is essential when it comes to thinking of future scenarios of our living. we need guides or imaginations that not only give “hope” or “anxiety” but create ideas of how you can push to make changes happen in real life. The principal idea of the speculation is that we imagine and make scenarios of our living in totally different situation such as itinerant living, shared living, living alone, etc..., so that we could see the living separated from the reality of what you can do.

At last, projection of the co-designed and co-speculated ideas is needed to make changes happen in real life. How to deliver the output is important not to end just with the ideas but to be able to impact in real life.

<br/>

<iframe width="560" height="315" src="https://www.youtube.com/embed/kVBD3iznuEE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Feedbacks from the juries

- Rethinking what has been "Living" for myself.
- Not scratching the surface of the theme, but dive in it.
- Deeper research to be able to think possible futures.
- An Ikea Catalog From The Near Future Laboratory <http://ikea.nearfuturelaboratory.com/>.
- Ithaca's poem <http://www.cavafy.com/poems/content.asp?id=259>.
- Identification of technology for the projection.
- Possible design of a block instead of a private living space.
- Hierarchy of the use of spaces in a housing : Relation between social hierarchy and spatial hierarchy.
- Catalogue of living space design by different disciplines

##### ![]({{site.baseurl}}/Futures of Living poster A1 2.jpg) (Credit: Ryota Kamio)

<br/>

##### ![]({{site.baseurl}}/Multiscalar scenarios map A5 2.jpg) (Credit: Ryota Kamio)
