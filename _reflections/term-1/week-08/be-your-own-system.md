---
title: 08. Living with Ideas
period: 19-25 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

#08. Living with Ideas
#### by Angella Mackey, David McCallum and Oscar Tomico



### Summary
This week's lectures have probably been the weirdest experience that I have ever done in my career. Speculating my idea, living with it, creating a magic box, etc… I must admit that I was panicked at first, however this week gave me a lot of lessons about how important action research is in design.


### Speculation

![]({{site.baseurl}}/W8_photo1.png)  


In the book, Speculative Everything; Design, Fiction, and Social Dreaming (2013) by Anthony Dunne and Fiona Raby, it defines that "Speculative design contributes to the reimagining not only of reality itself but also our relationship to reality". What we have learned this week does not necessarily link to the so-called speculative design, however the speculation has been positively practised in many fields.  

"speculative design can flourish—providing complicated pleasure, enriching our mental lives, and broadening our minds in ways that complement other media and disciplines. It’s about meaning and culture, about adding to what life could be, challenging what it is, and providing alternatives that loosen the ties reality has on our ability to dream. Ultimately, it is a catalyst for social dreaming" (A, Dunne and F, Raby, 2013).

During the lectures I found the speculation process an experimentation with yourself with what you wish to design or reach. Most importantly you become the first actor in your sepculation in order to know real feeling, texture, feedback from others, etc... It's also about doing research and design at the same time. Kurt Lewin, the father of action research, describs the Action research as "a comparative research on the conditions and effects of various forms of social action and research leading to social action" that uses "a spiral of steps, each of which is composed of a circle of planning, action and fact-finding about the result of the action"." in his paper, Action research and Minority Problems (1946).

Therefore, the both speculation and action research encourage a designer or researcher to intervene in the reality in order to design your social dreams that you aim to design.


### Learning by Speculating

 Day #1 Speculation by an Everyday object



I picked a Swiss army knife and speculated it as future portable-police. It's a speculation of what if there will be no police system in the future and we should protect ourselves. I attached a letter saying: "Under the constitution of Spain, Law No.XX this equipment allos the holder to prontect him/herself by all means, which includes damaging critically or even, killing". Apparently it's en extrem speculation.

Once I walked the streets with the Swiss army knif attaching with the letter, I felt an uncomfortable feeling with not what it says in the letter but the fake badge of the Police I made at a coffee shop. This might be the real feeling that you get uncomfortable with what you didn't expect, or you expected to feel uncomforable with something else first.


 Day #2 Speculation by Wearing the Digital

![]({{site.baseurl}}/W8_photo2.JPG)

It was time of reframing for me. Through the Greenscreen App we saw what we don't see through our own eyes and it was simply a fun moment. Firstly I intended to explore what possibilities there are with this App and little by little tended to focus on more detail of the functions, for instance from a jacket to a small piece of fabric.

 Day #3 Speculation by Making a Magic Machine

I must say this exercise was the most complicated for me, since I struggled a lot with reflecting my idea into a product (= a magic machine in this case). I found myself lack of just "playing" on the first stafe of design to explore the possibilities of my design options. However, at the same time I enjoyed this dilemma I had during the Magic Machine exercise because I felt that it pushed me to improve my design skills.


### Reflection on my final project

On the last 2 days of the lectures we worked on the speculation process of our own final project. My case was about housing or living in city environment. It was challenging to speculate, yes.

After talking to the lecturers I decided to sleep over at someone's place to experience shared living. My speculative hypotheses were 2 ways: 1) People start to live as an itinerant moving among different places to live and 2) People start to share living spaces, goods and experiences a lot more than ever. I chose the 2nd one as I thought it would promote to expand human living spaces with more energy consumption and most importantly more use of land.

First of all, this experimentation should be celebrated for a longer time when it comes to use it as a research material for the final project, however the intention was to observe the real insights you feel as the principal actor. The insights I got from the experiment was that for such a short time like one night she/he doesn't mind sharing things even though she/he minds sharing actually, because it's just for a short period of time. Also, you try to communicate more often in order to know each other's personal spaces and actually we talked a lot over night from casual things to personal experiences and views. The learnig from the experiment was something you need before you talk about what you can share and can't share, therefore it was a great insight.   


### References

- Anthony Dunne and Fiona Raby (2013), Speculative Everything; Design, Fiction, and Social Dreaming, MIT press, USA. <http://readings.design/PDF/speculative-everything.pdf>

- Kurrt Lewin (1946), Action research and Minority Problems,  Journal of Social Issues, 2, 4, 34-46.
<http://dx.doi.org/10.1111/j.1540-4560.1946.tb02295.x>
 
