---
title: 05. Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

#05. Navigating the Uncertainty
##### by Jose Luis de Vicente (Sónar+D) and Pau Alsina (UOC)


## Reflection
This week's lectures have dealt with a wide range of "design” by two external lecturers, Jose Luis de Vicente (Sónar+D) and Pau Alsina (UOC- Universitat Oberta de Catalunya) and internal teachers, Mariana Quintero, Tomas Diez and Oscar Tomico.

### Time scale
What is Now? Time scale has changed rapidly, therefore you can't design without the measurement of Now that you're referring today more than ever. Mainly from these two external lecturers we learned how our design lasts in different time scales. Particularly from Jose Luis (Sónar+D) how our design in industry has damaged the planet and other animal populations and from Pau (UOC) how the design has formed our societies based on the ontological hierarchy.  

My thought throughout the lectures was that Do we really design what we ask for? or Are we designed by systems that a few of us (human) or nature have designed? Unfortunately our societies haven't followed sustainable developments for last decades in terms of nature and social cohesion, therefore today we are facing serious issues such as climate change, extensions of species, inequality, etc… We might need Nature centered design rather than Human centered design from now on… At the same time we've seen how powerful the act of design has determined our surroundings, which gives us a lot of insights to become a "good" designer. For instance, Jose Luis defined the main actors in design with Decreationists, Capitalists, Accelerationists, Extinctionists and Mutualists and Tomas added Optimists. Pau approached design as the producer of society with the presence of Black boxes, in which micro political actions create social morals.  

![]({{site.baseurl}}/W5 graphic2.jpg)

This week we've discussed about probably the biggest human-made design like society. Society and design have been interacting between them basically for all human history and the outcome of the design acts has appeared as civilization. During the week of Biology Zero (08.12.2018 - 12.10.2018) I learned the oldest and latest systemic design from bacterias on the planet, on the other hand we got a question by Mariana, What is ultimate interface of the 21st century? I might say religion is definitely one of the ultimate interfaces human have made not only in the 21st century but for centuries. Whether you practice some religion or not, today's societies and culture are based on them to a certain degree. For example, I visited the city of Rome, Italy on 26/10/2018 - 28/10/2018 and Rome is undoubtedly designed by catholicism. Almost at every corner of the city there are catholic monuments and symbols and particularly, Vatican is a city designed for the catholicism.

![]({{site.baseurl}}/W5 graphic1.jpg)

### What is my design?   
I've been wondering my vision (of the future) as a designer and so far, I'm seeing myself as an Urban Designer. Urban design includes design of infraestructures of the city such as pavements, public spaces, etc.. (so-called traditional urban design), however my interest as an Urban Designer is to design a city in holistic way not only with design of the physical infrastructures but also with design of the intervention or system. My current concern is to what extent we can apply Learning by Doing or Participatory Design to Urban Design. In other words, when it comes to dealing with actual people could we make make so many mistakes? I guess I'd need to learn more of Co-design or Community Design in order to skill up as a designer.    
