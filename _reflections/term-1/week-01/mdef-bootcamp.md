---
title: 01. MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---
#01. MDEF BOOTCAMP

##### by Tomas Diez, Oscar Tomico, Mariana Quintero and Chiara Dall'Olio


## What did you do that is new. What did you learn?


![]({{site.baseurl}}/IMG_2452.jpg)

First of all, the 2 intensive weeks of Pre-school in September were all new to me learning Arduino, 3D modelling and rendering and video edit.

I've been studying/working as a researcher in human geography and urbanism field in Tokyo, Budapest and Barcelona, therefore I had to change my mindset to designer or creator's one and this was my very first new change since the MDEF began.

The introduction on the 2nd day was such inspiring to know my classmates' backgrounds and interests. It's interesting to see that the majority of them are either communication designer or industrial/product designer, but there are some rare profiles such as economist, political scientist and human geographer too. Moreover, the gender balance of the MDEF (Women: 17 Men: 10) seems very relevant to design for emergent futures, since I believe that feminizing a number of systems that exist today is the key to designing a more inclusive and collective society.   

After the city safari and trash hunting in Poble-nou our team (= Saira, Silvia, Ola, Oliver, Ilja, Gabor and me) found that the neighborhood is still not livable and many places like Makers' labs are only known for the people in "the network". We understand that Poble-nou has been industrial neighborhood traditionally and has been trying to recover as a business innovation district, although it has evolved in different way than the 22@ plan by the regional government of Catalonia with organic Maker's movement. However, we detailed the relationship between those makers' places and the neighbors more than its economical growth. Our thought is that Poble-nou has always been a neighborhood where people come to work/study and leave after their jobs/classes, therefore the interaction among the neighbors is thinner than the rest of the city. It's the fundamental difference between Poble-nou and the first Superblocks pilot area, Gràcia district. We came to a conclusion that a sustainable or auto-sufficient neighborhood/city needs the integration of both workplaces, commerce as a service and neighbors.   


## How can your learning process be described?

- Sharing the knowledge
- Brainstorming fresh memories and enrich them by other's opinions
- Fieldwork
- Observation of how people interact outside of the school
- Reframing

## How is this relevant to your work?



The Boot camp week has dealt with a wide range of topics from environmental issues to textile.

![]({{site.baseurl}}/pace_layering.jpg)

It's fundamental to comprehend that each topic has different pace of changes. According to Stewart Brand’s Pace Layers diagram from, Nature is the slowest and Fashion is the fastest to change in our society. However, after we’ve discussed this week it became more clear that the pace between the slowest and fastest has gotten closer and closer, as they affect each other more directly than ever. And at the same time I recognized that technology is involved in each layer more than ever as well.

For instance, Oscar Tomico's presentation brought me a number of insights that his approaches to clothes as such personal items is relevant to a bigger scale issue like environmental issue. The wide range of themes helps us look at emergent future's issues on smaller and bigger scale and as a result helps us create holistic approaches.


## Reference

Brand, S (1999), The Clock Of The Long Now: Time and Responsibility, Basic Books; Revised ed. edition, New York.
