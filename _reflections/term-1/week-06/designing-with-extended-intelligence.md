---
title: 06. Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---
#06. Designing with Extended Intelligence
### by Ramon Sangüesa (Elisava) and Lucas Peña


### Summary
This week's lectures have explored the possibility of extended intelligence such as Artificial Intelligence and Machine Learning, especially in design discipline. At the same time, we have discussed largely about the ethical use of those emergent technologies by questioning What is intelligence? and Who designs the intelligence? Besides that, we explored the possible use of those technologies in a group of 4 people through the week and presented the prototype of our ideas and thoughts on the last day.


### What is Intelligence?
Yes, very basic but essential question to begin. We divided the argument into 2 key points: Symbols and Senses to define how intelligence works in comparison to Plato and Aristotle. Later we compared 2 well-known measurements of intelligence: Turing's imintaion game and Searle's Chinese room to help us define the intelligence. We in a group of 4 defined intelligence as "Not consciousness but information processing".

In terms of Artificial Intelligence, Stuart Russell and Peter Norvig proposed the definition of Artificial Intelligence in their publication, Artificial Intelligence: A Modern Approach, in 4 different approaches: Thinking Humanly and Thinking Rationally (top) and Acting Humanly and Acting Rationally (bottom). "The definitions on top are concerned with thought processes and reasoning, whereas the ones on the bottom address behavior”.


### What are around the extended intelligence?<br/>
<br/>
![]({{site.baseurl}}/W6_graphic1.png)


 #1 Potentiality

We, as a team of 4 people: Julia Quiroga, Silvia Matilde Ferrari Boneschi, Gábor Lászlo Mándoki and me, prototyped a digital intelligent agent which selects the cheapest trip to Shenzen, China under the best possible condition with personal preferences. Basically we intended to make the machine identify the cheapest and best trip based on the very personalised input through detailed setting for the trip such as A to B transport, meals, budget, accommodation, everyday habits, category of your interest, etc.. and your SNS accounts like Facebook, Instagram, WhatsApp, etc… For the qualitative data such as selection of flights, restaurants, hotels/hostels or sightseeing sites we decided to use only community based platforms like Tripadvisor to analyze the opinions of others. Therefore the task we give the machine is going to be collection of information from the platforms and analisi of the data and profile. As the extended service, we proposed to design your future trips based on the information the machine learns from the previous trips. At this point we like the users to select his/her information to share with the machine for this service and moreover, the machine and users co-decide under what condition the users share their information.

In the process of prototyping the service with those extended intelligence, we intended to maximize the potential of those technologies but at the same time through our personal experience we've stopped and talked if this could be a danger. Clearly ethical aspect was discussed very often and also data privacy was carefully taken into account.


 #2. Ethic

During the lectures we discussed a lot about ethical aspect in artificial intelligence or technologies in general. First thing that came up is who makes such technologies. When you look at founding members of AI at Dartmouth workshop, they are all White caucasian men. Whether they intend or not, there are already cultural biases or any sort biases (which is completely normal and practically happens in any technology). On the October 30th I attended Exploring diversity in AI workshop organised by AllWomen.tech, where there was similar discussion about how gender equity matters in technologies, especially in artificial intelligence. For example, in Latin language group such as Spanish that has masculine and feminine forms often AI doesn't recognise feminine ones in processing information. It's quite clear that those emergent technologies reflect ethical views of the engineers or computer scientists who makes them, whether they intend to or not. Therefore, the diversity matters who makes and operates the artificial intelligence if we try to be ethical in AI.

Moreover, after talking personally to Lucas I found out that a number of issues with those technologies are not only technical issues but social issues. Particularly the ethic is mostly a matter of society more than technical evolution. On the other hand, according to Joi Ito (MIT) still languages of computer science differ a lot from the ones of social science, law or philosophy, even though there have been intention to understand each other. This is one of the reasons why even the latest technologies fail to solve a social issue, since most of the issues we have today are social. I've learned that technology is just a tool to help solve problems in a society, even though today machines or robots can "think" themselves and "grow" alone. Also, it's essential to think for what we use those technologies. In other words, I see that we have tended to think what we can do with the present technologies more than for what we need new technologies to solve problems we have: Identification of problems < Use of technology.   


 #3. Energy

At the last class we shortly discussed about how the present and emergent technologies affect in terms of energy consumption. Operation of the emergent technologies apparently cost a lot of energy and this fact should be taken into account more. For instance, according to Forbes "U.S. data centers use more than 90 billion kilowatt-hours of electricity a year, requiring roughly 34 giant (500-megawatt) coal-powered plants. Global data centers used roughly 416 terawatts (4.16 x 1014 watts) (or about 3% of the total electricity) last year, nearly 40% more than the entire United Kingdom. And this consumption will double every four years". You can easily imagine that an artificial intelligence or machine learning center need a lot more energy than the current data center. Hence, artificial intelligence engineers or designers must think about how to cover the massive energy use. It might be renewable energy or self-sufficient robot, however it's urgent to think about it as soon as possible.     


### Conclusion

Artificial intelligence or Machine learning have definitely a big potential to accelerate technical breakthroughs and the use of them should be well considered in many fields. However, we must be very mindful and careful with the introduction of those emergent technologies at the same time. In particular, in cities artificial intelligence could change radically its urban fabrics. How? For instance, the first possible artificial intelligence which could be introduced to the cities is going to be self-driving car. Self-driving car obviously will change urban mobility and its infraestructures. It's a matter of time when self-driving car passes safety exams and could drive in public roads. We need discussions about who takes responsibility in case of accident or hacking or even, if we need more cars in cities even if you don't drive and it comes with renewable energy. On the contrary, we also need to discuss and explore about under what condition those technologies are useful and help us better our lives in holistic manner.         


### References

- Forbes (Dec 15, 2017): Why Energy Is A Big And Rapidly Growing Problem For Data Centers, <https://www.forbes.com/sites/forbestechcouncil/2017/12/15/why-energy-is-a-big-and-rapidly-growing-problem-for-data-centers/#710676005a30>

- MIT Media Lab (Jan 10, 2017): Artificial Intelligence: Challenges of Extended Intelligence, <https://www.youtube.com/watch?v=KF9ZqnEiSzU>
