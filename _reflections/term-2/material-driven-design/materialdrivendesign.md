---
title: Material Driven Design
period: 24 January - 21 February 2019
date: 2019-02-07 12:00:00
term: 2
published: true
---

### Material dialogue (January 24th)

After Mette's theorical lecture about material driven design and circular economy we went to Valldaura to practice the material driven design.

In Valldaura we went to look for materials in the forest in order to understand the environment and ecosystem where the materials come from.

![]({{site.baseurl}}/MDD_W1.jpg)

I picked a piece of laurel wood as my material and (tried to make) made a spoon out of it. First I pealed off the skin and the more I pealed the more the nice smell came out.

![]({{site.baseurl}}/MDD_W2.jpeg)

While I was pealing the skin, I found a sizable split and I decided to split the wood into 2 pieces following the split. Since then the size of the spoon differed than I expected and at the same time decided to follow the material's lines. Since there was a moment in which the material didn't allow me to cut the way I wished and I cut myself by trying too hard to cut, I started to follow the material's shapre as much as possible to make my work easier.

![]({{site.baseurl}}/MDD_W3.jpg)

Also I entred "meditation" mood in which I just worked on the craft unconsciously and time flied.

![]({{site.baseurl}}/MDD_W4.jpg)


### Exploring new materials by bio-waste (January 31st - February 21st)

After considering several materials and having talked with Mette and Tomas about them I decided to work on the bio-waste generated in the process of making beer.

The reason why I'm interested at workinh on the beer waste is that first of all I've been interested at alchol industry in general and secondly the consumption of beer is relatively universal wherever you go. In fact, beer is the 3rd most popular beverage after water and tea. Therefore, I find there is a big potential to design a circular economy model in the beer industry, if possible new material can be found.

Another practical reason is that regular beer making is quite simple and made by basic and few raw materials: water, spend grains (malt), hops and yeast. That helps us a lot to reduce the time of removing unnecessary parts of the ingredients and simplify the research.

### Experimentation / Explorations

First thing we came up in our mind was that we have to dry the spent grains out as soon as possible, or they started to smell strong and acid.   

 #1 We left the malts on the terrace of IAAC and in the oven for 3-4 hours.

 #2 Grinded the dried malts and powered them

 #3 Mixed the powered malts with yeast from the brewery.

 #4 Put it in a flat and thin shape

 #5 Leave it in the oven for 2-3 days while we tested the strength every hour but they didn't differ much per hour.

In order to dry the spent grains we have tried it both in the oven and in the roof of IAAC under natural sunlight, but the grains under the sunlight started to get fermented.

A part from that I have boiled and heated the spent grains and grinded them in different sizes. Also, we prepared a sheet of the dried spent grains in the oven of 50°C every hour to see its strength and smell.

For the stickness of the materials I decided to add 5g of tapioca, which is 5% of the whole ingredients during the last week.   


### Receipes

Indredients:

- Spent grain: 162g (56%)
- Boiled yeast: 80g (27%)
- Water: 29g (10%)
- Tapioca: 15g (5%)

  TOTAL: 286g

Step 1. Drying the spend grains. You can dry them under the natural sun or in the oven.

Step 2. Grinding the dried spent grains and powederig them by mesh strainer.

Step 3. Boiling the liquid of water, hops and yeast untill it gets sticky glue.

Step 4. Mixing the powedered spent grains and cooked tapioca with the yeast glue to make "clay".

Step 5. Pushing the liquid put from the mixed clay by metal mesh sheets.

Step 6. Put the clay in the form made by the metal mesh sheets or a module and put it in the oven of 50°C for a day (more than 20 hours).

Step 7. Taking the clay carefully from the mesh sheet (or you can cut it).  


![]({{site.baseurl}}/MDD_B1.jpg)

![]({{site.baseurl}}/MDD_B2.jpg)

![]({{site.baseurl}}/MDD_B3.jpg)

![]({{site.baseurl}}/MDD_B4.jpg)

![]({{site.baseurl}}/MDD_B5_3.jpg)

![]({{site.baseurl}}/MDD_B5.jpg)

![]({{site.baseurl}}/MDD_B5_2.jpg)

![]({{site.baseurl}}/MDD_B9_1.jpg)

![]({{site.baseurl}}/MDD_B5_4.jpg)

![]({{site.baseurl}}/MDD_B9_2.jpg)

![]({{site.baseurl}}/MDD_B9_3.jpg)

![]({{site.baseurl}}/MDD_B10.jpeg)


### Experiences

With the limited time to explore my materials I have intended to stick to the original materials from the breweries without any additional elements such as starch or chemical products, which has pushed me to play with the materials in so many different ways with the form, temperature, portion and the way to explore them. In the middle of the explorations I found that powdering the spent grain is the most solid form and the yeast out of the liquid waste could use as a binder.

Later, in the process of making clay out of the mixed spent grains and yeast there was a point in which I could guess, more or less, if the clay works or comes out in good shape after the oven. In particular, the texture and color of the clay taught me how solid the clay is. This was the point for me to control the materials.

As personal experience I've enjoyed so much exploring materials which is a new discovery for me that I really didn't expect at the beginning. For the next step, I'd like to think more on what kind of tools can be used and explore more complex shapes than what I have done.  


### Lessons learned

Lessons that I have learned during the material driven design are also applicable for my final project.

At the beginning of the experimentation I didn't know where to start and couldn't picture the outcome at all, since I had never worked on material or biological experiments before. So, to facilitate the experiments I set a condition that uses only wastes from the brewery and in that condition I decided to "play" around the materials. At this point the important lesson for me was that when it comes to designing something you can't picture the outcome you just start to touch, feel and play with the materials you identify. Moreover, setting conditions helps you concentrate the potentials of the materials and also often helps the experiment more profound.

When you start to control the materials you can imagine the possible narratives from the possibilities that you have found through the experiments. Making narratives at this stage, at which you don't understand the materials completely but see the potentials is quite important, as you don't determine or limit the narrative within your knowledge of the materials. Here making narratives pushes you to think of the use of the materials on different scales in a circular manner and look at your materials more critically. On the other hand, the more you know the possible potentials of the materials the easier to sharpen the narrative.

In particular, in futuristic or speculative design projects this type of material/ingredients driven design or reverse design methods are very helpful. 
