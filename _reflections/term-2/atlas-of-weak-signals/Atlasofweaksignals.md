---
title: Atlas of Weak Signals
period: 14 March - 11 April 2019
date: 2019-03-14 12:00:00
term: 2
published: true
---
The lectures of Atlas of Weak Signals are divided into Definitions by Jose Luis & Mariana and Development by Lucas & Ramon.

During the 5 weeks we have explored to identify weak signals and learned methodologies to collect them in an effective way. Among the 25 weak signals during the lectures I have personally worked on 1) Circular Data Economy, 2) Fighting Anthropocene Conflicts (movement of species), 3) Tech for Equality and 4) Rural Futures. A part from that we, Futures of Cities group, have presented The New Citizen System as final presentation of the Development lecture.

### Weak signals around my final project

I developed mapping of weak signals (some are from the lectures and others not) for my final project. And by the first mapping I developed another mapping focusing on my area of interest/intervention to see what signals are around my project.

![]({{site.baseurl}}/Mapping 1.png)
![]({{site.baseurl}}/Mapping 2.png)

I personally enjoyed most working on 2) Fighting Anthropocene Conflicts (movement of species), in which explored how extinction/movement of species would affect to our urban living and speculated from the movement of species to climate refugee. This exercise supported one of the narratives why I have to address my area of interest, Futures of Living.

As a matter of fact, British environmentalist, Norman Myers suggested that by 2050 around 250 million people will migrate to somewhere from their cities or countries by climate changes. Climate change already answers that today’s competitions between cities or nations based on the consumption of natural resources is not sustainable. When we look at weak signals from the environment it’s clear that the expansion of cities is no more an option today.

### Reflection

We have learned interesting tools to facilitate us to collect weak signals and visualize them such as Data scrapping via Python and KUMU (you can see the mapping of our identified Weak signals developed by Saira Sraza <a href="https://kumu.io/sraza/materials6#aowsscraped">here</a>), and also through making our own mappings we have tried to define the definition of each weak signals and interact them in a circular manner in the Development lectures. On the other hand, in the Definitions lectures we have learned both current and emergent weak signals to help us explore how our projects will be positioned in the emergent futures.

From my personal perspective this course might have been even more helpful for many of us in the 1st term, where we worked a lot to contextualize our areas of interest toward our final projects and make narratives out of the weak signals. It might have been a great foundation of story/narrative making.   


### Weak signals that we have dealt with during the course

Attention protection / Dismantling filter bubbles / Circular data economy / The truth wars (Fake news) / Redesigning social media / Manipulation / Longtermism / Fighting anthropocene conflicts (movement of species) / Carbon neutral lifestyles / Interspecies Solidarity / Climate Consciousness / Future Jobs / Human-machine creative collaborations / Fighting AI Bias / Tech for equality / Making UBI work / Making world governance / Rural futures / Pick your own passport / Refugee tech / Non heteropatriarchal innovation / Imagining Futures that are non western centric / Reconfigure your body / Gender Fluidity / Disrupt ageism / Metadesign to reclaim the technosphere
