---
layout: page
permalink: /about/
---

![]({{site.baseurl}}/Aboutme.gif)


I'm a Japanese urbanist based in Barcelona. Currently at Institute for Advanced Architecture of Catalonia (IAAC) designing for emergent futures. My research focus has been on the relationship between urban policy, public space and consumption in cities. I joined Master in Design for Emergent Futures in 2018 to give solutions to the current and emergent issues related to the topics mentioned above...

The United Nations says that today 55% of the world’s population lives in urban areas, a proportion that is expected to increase to 68% by 2050. So, my current interest and concern is housing, since it has become a crucial concern and even a threat to citizens although it's fundamental for city living.

### #Shared_living #Gentrification #Housing_bubble #Itinerants etc...

# How are we dealing with it?
