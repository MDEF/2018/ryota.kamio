---
layout: shenzhen
title: Shenzhen research trip
permalink: /shenzhen/
period: 09-14 January 2019
date: 2019-01-15 12:00:00
published: true
---

### Shenzhen, China
#### (08.01.2019 - 14.01.2019)

### #1 Shenzhen

<iframe width="560" height="315" src="https://www.youtube.com/embed/6s8CW2JNlr8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Shenzhen has experienced a radical transformation from fishermen village to the largest metropolitan area in China over the last decades, especially since it became China's first Special Economic Zone in 80's. With the rapid economic and population growth the city has changed physically and demographically, and the massive urbanization has taken place for the coming immigration from all over China to the city.

#### ![]({{site.baseurl}}/images/sz3.jpg) (Airbnb where we stayed in Shenzhen)

Urban villages are an interesting fact of the unique urban development of Shenzhen, in which "Once-rural landowners organized into cooperatives and transformed their real estate into urban villages built on dense, informal, and relatively cheap accommodation - targeting the massive number of migrant workers who have made Shenzhen their home(<a href="https://www.sixthtone.com/news/1002091/why-urban-villages-are-essential-to-shenzhens-identity">Hartog, H</a>)". Actually the urban villages differ from one to another quite much, and some have completely changed their figures to skyscrapers or European style residential area and others have kept the old landscape. In terms of mobility the metro is well connected and runs all over the city center for very affordable price. Particularly, electric bicycle and electric taxi seem a common transport in the city.    


### #2 Shenzhen model

![]({{site.baseurl}}/images/sz1.jpg)  

The characteristics of Shenzhen could be seen from many perspectives, but the speed of prototype and production in the software industry is totally remarkable. Hacking culture makes this rapid prototype possible, therefore the more the products are hacked, imitated or copied the more valuable they become (which in the West it's not common). "In Shenzhen, the community strongly believes in the principle of the freedom to create, explore, and invent. Shenzhen’s makers live by open source hardware and software, where people share information online on how to build the inventions they have released on the market. That information is then used by the rest of the community to modify and improve upon. The whole purpose is that everyone has access to the information, and everyone and anyone can modify it. This is what is referred to as the “Maker Movement (<a href="https://hackernoon.com/shenzhen-china-where-tech-dreams-come-true-718cf13a6913">hackernoon</a>)".

![]({{site.baseurl}}/images/sz2.jpg)

With the great development of digital payment platforms such as WeChat and Alipay, the consumption model has been digitalized radically, and it's not only in the developed areas of the city but practically everywhere from a small fruit shop to shipping mall. At the same time other types of digital services such as food delivery, video sharing, or on-line booking have emerged, therefore the digitalization undoubtedly has shaped a new consumption model and impacted the society as a whole.

![]({{site.baseurl}}/images/sz4.jpg)

Apparently all these changes have generated changes in the way of living in this city. When it comes to urbanization, it seemed that new buildings preserve more privacy spaces in comparison to urban villages and the digital platforms make the individualistic living more comfortable. The interesting contrast I found during my stay in between "developed" and "non-developed" urban villages was that the services the both places have are quite similar, however during weekdays the non-developed urban village is constantly filled with a number of people during the whole day and the use of its public space is so diverse that parents leave their kids with food on the streets, elderly people play Chinese chess with tea, construction workers take naps, etc... while the developed one is mainly used for weekends. As I observed the non-developed urban village often has fewer private spaces and lacks some equipments, hence the residents seem to look for the functions lacked in their places in the public spaces. In other words, their living spaces are extended to the public spaces, so that the public spaces are used a lot more actively than the developed urban village.               

As Eric Pan (the CEO and founder of Seeed Studio) pointed put in the session, there are many spaces for makers or designers to fill such gaps or niche problems. It was a great reflection for me as a designer to look at a wicked issue like living and to design the interventions.

### Group presentation: Public engagement&Matters of concern

Team member: Katherine Vegas, Vicky Simitopoulou, Julia Quiroga and Ryota Kamio

<iframe src="https://player.vimeo.com/video/322255081" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/322255081">MDEF research trip in Shenzhen (China)</a> from <a href="https://vimeo.com/user91177114">Ryota Kamio</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

![]({{site.baseurl}}/images/Slide01.jpg)
![]({{site.baseurl}}/images/Slide02.jpg)
![]({{site.baseurl}}/images/Slide03.jpg)
![]({{site.baseurl}}/images/Slide04.jpg)
![]({{site.baseurl}}/images/Slide05.jpg)
![]({{site.baseurl}}/images/Slide06.jpg)
![]({{site.baseurl}}/images/Slide07.jpg)
![]({{site.baseurl}}/images/Slide08.jpg)
![]({{site.baseurl}}/images/Slide09.jpg)
![]({{site.baseurl}}/images/Slide10.jpg)
