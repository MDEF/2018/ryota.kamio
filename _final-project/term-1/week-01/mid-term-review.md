---
title: 01. Mid-term review
period: 05 March 2019
date: 2019-03-05 12:00:00
term: 1
published: true
---

Since the <a href="https://mdef.gitlab.io/ryota.kamio/reflections/design-dialogues/">Design Dialogues</a> of the 1st term my journey has been challenging, but at the same time I've seen a certain progresses too.

The main difficulty of this journey has been how to narrow down such big topic: Futures of Living. The starting point of this project was to explore the gap between so-called shared living and individualistic living. The shared living or communal living has been promoted as an alternative life style especially in the context of growing population in cities, but the individualistic reality that we live in is so far from what the shared living aims at. Therefore it seems to me that there have been certain gaps which we don't get attracted by even though the idea sounds very reasonable.

#### ![]({{site.baseurl}}/shenzhen.jpg) (Credit, Ryota Kamio)

Once I talked about my final project with a friend from TU/Delft he gave me an insightful advise, that when it comes to making a speculative design project often setting the outcome or targeting an object helps you make the clear narrative. Since then I have been looking at futures of every day's objects we use at home such as key, door, window, partition, etc...

_Actually this process reminded me of the experimentation of <a href="https://mdef.gitlab.io/ryota.kamio/reflections/materialdrivendesign/">Material Driven Design</a>, in which you just touch the ingredients and play with them with no image of the goal. This process allows you to explore the possibilities of the materials and have a profound knowledge of them before making a narrative_

During identifying the objects around housing I found that living room is a space designed for collective use. Basically living room is designed for family or house members to mingle and share leisurely activities or to receive guests, therefore the layout and furnitures of living room are mainly designed to facilitate communication and interactions.

On the other hand, the living room might be the first one to disappear in future housings, since there is no "productive" use of it such as kitchen or bathroom. For those reasons I thought it'd be interesting to think new uses of the living room as a collectiveness in urban living.

#### ![]({{site.baseurl}}/House history_Ryota.png) (Living rooms in the apartments I have lived in Barcelona since 2014 - Credit, Ryota Kamio)

Firstly I started to analyze my own living rooms in the apartments where I have lived in Barcelona since 2014 as personal experience of me as an immigrant. At the same time researching the history of the living room and its social and cultural context has brought me the ideas of living room as a space which allows you to spend non-productive time.

In order to imagine futures of living room I started to collect some examples of extremely absurd uses of the living room. The purpose of this exercise is to reframe the current state of the living room in speculative ways to observe it from different perspectives. At this point I picked "Kitchenless City" by a Catalan architect <a href="https://www.maio-architects.com/project/kitchenless-city/">Anna Puigjaner</a> as a reference to develop my project, in which describes the role of kitchen from perspectives of gender, anthropology, politic and economy.      
