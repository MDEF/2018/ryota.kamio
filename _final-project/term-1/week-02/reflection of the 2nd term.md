---
title: 02. Reflection of the 2nd term
period: 09 April 2019
date: 2019-04-09 12:00:00
term: 1
published: true
---
You can see the <a href="https://gitlab.com/MDEF/ryota.kamio/blob/master/_final-project/term-1/week-02/Futures%20of%20Living%20report.pdf">report</a> and <a href="https://gitlab.com/MDEF/ryota.kamio/blob/master/_final-project/term-1/week-02/newspaper-CS4.pdf">speculative newspaper</a> in PDF.

Also the video <a href="https://youtu.be/4gRq_xWdR6A">here</a>

![]({{site.baseurl}}/futures of living.png) 


### Summery

The final presentation of Design Studio for the 2nd term. Design Dialogue was held on the April, 9th 2019 at IAAC. Design Dialogue is a space where students of the Master in Design for Emergent Futures get feedbacks from both internal and external guests on their final projects.

On the 2nd Design Dialogue day I presented a speculative newspaper, a report and a video in which explains design actions I have taken. Since the mid-term review on March 5th I have developed my final project by interviewing, speculating, mapping and trying to find an intervention in my area, Futures of Living.

Though the journey I have identified some possible interventions to address the importance to discuss about our futures of living from different perspectives and narratives. The one is futures of key which aims to design new ways of protecting our privacy in collective livings. It aims to design a “key" that not only closes and opens, but creates trust and privacy in collective manner. The idea came through researching co-living and I found out that lack of privacy has been the biggest concern when it comes to practicing co-living/shared-living ideas. The another idea is to design a “curiosity" to trigger to attract people in a collective place. Since I found living room as a representative collective space in a housing I began to observe many different collective spaces with no concrete utilities in cities such as plaza (square). In Spain the plazas represent collectiveness of the city and neighbourhood and normally they’re used as living room for the community, and I found interesting that there is a fact that there are very much populated plazas and empty ones for environmental, cultural, locational, commercial and phycological reasons. Therefore, in order to grow collectiveness in cities I think such phenomenon of attracting people by people is worth to research and design possible triggers to do so.


### Reflection

During the Design Dialogue I received feedbacks from Ingi Guðjónsson (IaaC), Jonathan Minchin (IaaC), Chiara Dall’Olio (IaaC), Marta Handenawer&Pol Trias (Domestic Data Streamers), Andres Colmenares (Internet Age Media) and Lucas Peña (MDEF faculty).

The feedbacks I gained from both the internal and external guests were largely same, therefore it made me clear with what I should develop from now on. For instance, I got a lot of positive feedbacks in narratives and contextualization of my project. Many said that my narrative is solid and convincing why we should address to think and design the futures of living more actively from environmental, social, political and cultural points of views. On the other hand, I got the same amount of feedbacks that it has not defined well what my intervention is… what my “pitch” is…

I have been struggling a lot with finding spots to get my hands on, and actually I intended to take advantage of this day to find possible collaborators and contacts to design my intervention by action-researching in real life. In my case the so-called crystallized pieces of the issues around livings have not been so difficult to find on both meta and micro scales, but engaging with the people, communities or spaces with such issues in order to intervene has been challenging, especially for such a short time, (although it’s so natural to have such problems because I basically ask them to be materials for my project with no promised solutions for them. And It’s been an important lesson for me to learn how to include non-designers in a social-design project).

I got recommendations to contact several entities and non-profit organisations that deal with homeless people’s issues in Barcelona or refugees in Europe. Therefore, the next action for me is to set meetings with them and hopefully I could find spaces to get my hands on in order to design an intervention by action-researching. In case that none of them works I have a Plan B to develop a design fiction or manifesto for futures of collective spaces in cities.

During the Design Dialogue I received a number of great feedbacks and insights. In order to articulate here I document most interesting ones from each guest ->

- Ingi Guðjónsson (IaaC)
"Identify activities that are difficult to make happen individually but easy with others to trigger collective behaviours."

- Jonathan Minchin (IaaC)
"List and make statistics and infographics of collective activities that don’t exist as commerce today."  

- Chiara Dall’Olio (IaaC)
"Find a specific space/community/people or collaborators."

- Marta Handenawer&Pol Trias (Domestic Data Streamers)
“Observe spatial architecture to obtain insightful hints for future collective spaces.”

- Andres Colmenares (Internet Age Media)
“Make a futuristic design fiction combining my knowledge of urbanism and the explorations taken place during MDEF in more visual manner.”

- Lucas Peña
“Plan to develop the manifesto of futures of living as a backup plan, since the area of intervention is not clear enough.”


### Intervention proposal

- 22 - 28 April
Meeting with the contacts who I got from the Design dialogue day, and ideally finding possible work spaces.

- 29 (April) - 5 May
Field work and action researching in the identified work spaces. Specifically finding urgent issues in the spaces and analyzing them.   

- 6 - 12 May
Speculative design exercises with objects or story telling. Filming all the process and analyzing the result.

- 13 - 19 May
Starting to document the project with diagrams, statistics and infographics. Starting to collect materials for the video and presentation.  

- 20 - 26 May
2nd speculative design exercises learning things to better from the 1st time. Workshop with the proposals of futuristic collective spaces in cities.

- 27 (may) - 2 June
Preparation for the presentation (video, diagrams, web, etc…).

- 3 - 9 June
Setting up the presentation and inviting people might be interested at my project to the presentation.
