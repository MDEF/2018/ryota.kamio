---
title: 07. Electronics Design
period: 06 March 2019
date: 2019-03-06 12:00:00
term: 1
published: true
---
### Week 7 assignment was to design and program a PCB using use the last weeks FAB ISP with at leat one output and one input.

The process of designing and programming your own PCS is following:

1) Designing the PCB on Eagle software (AutoCAD).

2) Milling the PCB board on CNC machine.

3) Programming the PCB via Arduino application.

The required parts are:

- 6-pin programming header
- Attiny44A (Microcontroller)
- FTDI header
- 20MHz crystal
- Resistor 10k ohm x6 (pull downs)
- Resistor 499 ohm x2 (for LEDs)
- Capacitors 2 x 22 pf capacitors
- Switch
- LED

### Designing your PCB

To design a PCB board we were introduced to use either Eagle or KiCad. This time I decided to use Eagle to design my own PCB, as it was slightly easier for me to utilize.

![]({{site.baseurl}}/Eagle1.png)

First of all, in order to add the right components I installed Fab Library by pressing "Add part". Therefore, basically all the components to place on my PCB board are from this library. After the schematic is completed, move on to switching to board view to arrange the components and their routes.

![]({{site.baseurl}}/Eagle2.png)

Routing the components is probably the most manual work on Eagle, but at the same time it helps you understand how the components work by routing one to another. In the routing you find some spots where you can't avoid overlaps, so you have to place resistors as a jumper. The less jumpers the better, apparently. Here you need your design skill to arrange the routes in minimalistic manner. (I finally ended up adding 6 resistors...)

Once you think it's done troubleshoot your board and recheck the errors. The errors appear in red and you have to fix them manually (here the jumpers appear as errors but that's fine). When all the errors get fixed it's ready to mill and solder the board as the same steps as <a href="https://mdef.gitlab.io/ryota.kamio/fab-academy/electronics-production/">W5 Electronics production</a>.

#### ![]({{site.baseurl}}/Ryota_Elecronic Design.png) Final PCB design
#### ![]({{site.baseurl}}/electronic design outline.png) Outline

#### ![]({{site.baseurl}}/PCB soldering.jpg) The soldered PCB

During soldering I realized that I forgot to cut the outline... however I didn't want to risk the board to be broken, so I decided to leave it like that (even though I tried to cut it manually).

You can download the [**Eagle - Board file**]({{site.baseurl}}/Electronic design_Ryota.brd) and [**Eagle - Schematic file**]({{site.baseurl}}/Electronic design_Ryota.sch).
