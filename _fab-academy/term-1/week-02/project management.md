---
title: 02. Project management
period: 23 January 2019
date: 2019-01-23 12:00:00
term: 1
published: true
---
### Git concept

According to Wikipedia, "Git Git-logo.svg A command-line session showing repository creation, addition of a file, and remote synchronization A command-line session showing repository creation, addition of a file, and remote synchronization Original author(s) Linus Torvalds Developer(s) Junio Hamano and others Initial release 7 April 2005; 13 years ago Stable release 2.20.1 / 15 December 2018; 50 days ago Repository git.kernel.org/pub/scm/git/git.git/ Edit this at Wikidata Written in C, Shell, Perl, Tcl, Python Operating system POSIX: Linux, Windows, macOS Available in English Type Version control License GPLv2, LGPLv2.1, and others Website git-scm.com Git (/ɡɪt/) is a distributed version control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows."

Since we created our Gitlab website during the 1st semester of the Master in Design for Emergent Futures (2018-19), I added "Fab Academy 2019" section in the web that I already made based on Jekyll.

However, here I briefly explain how I made a Gitlab webiste:

1. Create account on gitlab
2. Make a SHH key: ``ssh-keygen –t rsa –C “$my_email”`` + ``cat ~/.ssh/id_rsa.pub
`` in your Terminal (Mac)
3. Clone your repository in your own computer: ``cd “path to the folder” `` + ``git clone`` in yor Terminal
4. Choose the editor. In my case I decided to use Atom.
5. Most of MDEF students also use Jekyll to facilitate to structure our website


This time basically I duplicated "reflection.html" on layouts file and added "_fab-academy.html_" to add the same structure for documentation for Fab Academy 2019.

![]({{site.baseurl}}/W2_pic1.png)
#### ![]({{site.baseurl}}/fab-academy.png) fab-academy.html

At the same time, I duplicated other related files "_fab-academy.md_" file and "_fab-academy_" file in the same way.

#### ![]({{site.baseurl}}/md file.png) fab-academy.md

Later I modified "_header.html_" and "_config.yml_"

#### ![]({{site.baseurl}}/header.png) header.html
#### ![]({{site.baseurl}}/config.yml.png) comnfig.yml


### PUSH!
Once you have the files completed you have to push the files to your Gitlab (through Atom in my case).

So, first you have to stage newly modified or added files. The newly modified or added files automatically appear on the Git window.
![]({{site.baseurl}}/Stage all.png)
![]({{site.baseurl}}/Stage all 2.png)

Later you commit with the description of the added files such as name of the modification or additional information/file.
![]({{site.baseurl}}/commit.png)

And push them!
![]({{site.baseurl}}/Push.png)

It takes a few minutes to reflect on your web... you can check the process on your Gitlab page (CI /CD + Pipelines)
![]({{site.baseurl}}/pipelines.png)


And IT'S DONE!

![]({{site.baseurl}}/W2_pic2.png)

![]({{site.baseurl}}/W2_pic3.png)
