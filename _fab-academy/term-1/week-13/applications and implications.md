---
title: 13. Applications and Implications
period: 17 April 2019
date: 2019-04-17 12:00:00
term: 1
published: true
---
### Week 13 assignment was to propose a final project that integrates the range of units covered.

I'm a Master in Design for Emergent Futures (IAAC), therefore here I present my master project here.


### What is the scope of my project?
Shared living/Communal living/Co-living has been studied and designed actively as an alternative living for the emergent future by the rapid population growth in the cities. From environmental perspective shared living enables less consumption of energy by sharing the utilities such as kitchen, bathroom, etc… and naturally consumes less land. Moreover, especially for us, Millennials or Generation Z, it gives more opportunities to have contacts with other humans in person and to build social relationship.

With development of digital technologies we have been able to access to alternatives life styles that we couldn’t imagine before the internet era began in the 90’s. Nomad or remote working has made possible for us to choose basically wherever you want to work from. However, still cities have kept attracting people (by the accumulation of capitals) and as a matter of fact, urban population has been growing rapidly and undoubtedly.

### What will it do?
I'm now in the process of testing my ideas and through the results I'm developing prototypes. In order to test my project I have been planning to set installations in some homeless shelters or refugee camps in Barcelona.

The installations will be interactive with the participants, possibly with Arduino sensors or Python.

### What has done beforehand?
For most of the MDEF students we have been making narratives of our own project, which has been one of the biggest exercises. It has helped us develop our project in sustainable way in the future. Also it has helped me see my interest from different perspectives and on different scales.

Practically speaking, I have done some mapping exrcises through AI and Machine learning techniques throught the lectures of <a href="https://mdef.gitlab.io/ryota.kamio/reflections/Atlasofweaksignals/">Atlas of Weak Signals</a>.

![]({{site.baseurl}}/Mapping 1.png)
#### ![]({{site.baseurl}}/Mapping 2.png) Mapping of Weak signals around my projects

### what materials and components will be required? (where they come from? what it is the cost?)
For my project the main materials are people.
For the installations I've been planning I need to discuss with the partners, however so far some Arduinos, cardboard, my own laptop and possibly polywood are enough.

### what parts and systems will be made ? what processes used?
I'm using Fusion360 for some quick protypes and futuristic product as an intervention. Also Python might be used for data visualization.  

### What tasks need to be completed? what questions need to be answered?
My project is about new design of collective living in cities, therefore a part from technical requirements there are a lot of social, environmental and political changes that are needed to happen. And designing for emergent futures as it says in the name of the master requires a good amount of design-research and prototype&test in real life.

In terms of technical tasks, there should be a certain amount of data through the interactive installations and workshops.   

### how will it be evaluated?
Every single citizen in cities could basically evaluate my project, since it talks about futures of urban living. For the project I would let all the participants give me feedbacks and evaluations (if they wish to give me).
