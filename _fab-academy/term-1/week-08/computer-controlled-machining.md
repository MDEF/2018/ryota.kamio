---
title: 08. Computer-Controlled Machining
period: 13 March 2019
date: 2019-03-13 12:00:00
term: 1
published: true
---
### Week 8 assignment was to make (design+mill+assemble) something big by CNC machine.

CNC means computer numerical control. CNC milling is a "subtractive" method for processing raw material (workpiece or stock) with a drill-like rotating cutter (end mill) through a set of multiaxial, computer-driven movements of the cutting head.

This time I decided to design a chair for my apartment.

### Design

Firstly I started to sketch some design ideas.

![]({{site.baseurl}}/CNC_sketch.jpg)

For this exercise I liked to design something simple but purely from the scratch, therefore I decided to make a simple chair with only 3 parts. I measured a chair of our classroom as an example.

### Design on Fusion 360

This time I used Fusion 360 to design my chair.
I passed the design idea to Fusion 360, and once the design is finished passing the Fusion file to Rhinocamp.

![]({{site.baseurl}}/Measurement CNC chair.png)

### Milling

![]({{site.baseurl}}/IMG_4068.JPG)

![]({{site.baseurl}}/IMG_4069.JPG)

![]({{site.baseurl}}/IMG_4072.JPG)

Here you can download the [**file**]({{site.baseurl}}/Ryota_chair_CNC v14.f3d)
